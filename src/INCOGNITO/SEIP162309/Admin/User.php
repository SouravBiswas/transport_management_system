<?php
namespace App\Admin;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class User extends DB
{
    public $table = "admin";
    public $firstName = "";
    public $lastName = "";
    public $email = "";
    public $phone = "";
    public $address = "";
    public $password = "";
    public $id = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data = array())
    {
        if (array_key_exists('first_name', $data)) {
            $this->firstName = $data['first_name'];
        }
        if (array_key_exists('last_name', $data)) {
            $this->lastName = $data['last_name'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }
        if (array_key_exists('address', $data)) {
            $this->address = $data['address'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = md5($data['password']);
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

    }

    public function store()
    {

        $firstName = $this->firstName;
        $lastName = $this->lastName;
        $email = $this->email;
        $phone = $this->phone;
        $address = $this->address;
        $password = $this->password;
        $email_verified = 'Yes';

        $query = "INSERT INTO admin (`first_name`, `last_name`, `email`, `password`, `phone`, `address`,`email_verified`)
VALUES (?,?,?,?,?,?,?)";

        $dataArray = array($firstName, $lastName, $email, $password, $phone, $address, $email_verified);

        $STH = $this->conn->prepare($query);

        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            // return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            //return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function index()
    {

        $sqlQuery = "select * from admin WHERE email_verified = 'Yes'";

        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }

//end index

    public function delete(){

        $sqlQuery = "DELETE from admin WHERE id = $this->id;";

        $result = $this->conn->exec($sqlQuery );

        if($result){
            Message::message("Success :) Data has been deleted successfully.");
        }
        else {
            Message::message("Failure :( Data has not been deleted due to an error.");

        }
    }//delete()



    public function change_password()
    {
        $query = "UPDATE `user-management`.`admin` SET `password`=:password  WHERE `users`.`email` =:email";
        $STH = $this->conn->prepare($query);
        $result = $STH->execute(array(':password' => $this->password, ':email' => $this->email));

        if ($result) {
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Password has been updated  successfully.
              </div>");
        } else {
            echo "Error";
        }

    }

    public function view()
    {
        $query = " SELECT * FROM admin WHERE email = '$this->email' ";
        // Utility::dd($query);
        $STH = $this->conn->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();

    }// end of view()

    public function edit()
    {
        $query = " SELECT * FROM admin WHERE id = '$this->id' ";
        // Utility::dd($query);
        $STH = $this->conn->query($query);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleDate = $STH->fetch();
        return $singleDate;

    }// end of edit()


    public function validTokenUpdate()
    {
        $query = "UPDATE `user-management`.`admin` SET  `email_verified`='" . 'Yes' . "' WHERE `users`.`email` ='$this->email'";
        $result = $this->conn->prepare($query);
        $result->execute();

        if ($result) {
            Message::message("
             <div class=\"alert alert-success\">
             <strong>Success!</strong> Email verification has been successful. Please login now!
              </div>");
        } else {
            echo "Error";
        }
        return Utility::redirect('signup.php');
    }

    public function update()
    {

        $firstName = $this->firstName;
        $lastName = $this->lastName;
        $email = $this->email;
        $phone = $this->phone;
        $address = $this->address;
        $password = $this->password;
        $sqlQuery = "UPDATE admin SET first_name = ?,last_name=?,email=?,password=?,phone=?,address=? WHERE id =$this->id";
        $dataArray = array($firstName, $lastName, $email, $password, $phone, $address);

        $STH = $this->conn->prepare($sqlQuery);


        $result = $STH->execute($dataArray);


        if ($result) {
            Message::message("Success! Data has been updated Successfully!");
        } else {
            Message::message("Error! Data has not been updated");

        }


    }
}// end of update()
