<?php
namespace App\Admin\Driver;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class driver extends DB
{
    public $table = "driver";
    public $driverName = "";
    public $age = "";
    public $phone = "";
    public $busNumber = "";
    public $image = "";
    public $id = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data = array())
    {
        if (array_key_exists('name', $data)) {
            $this->driverName = $data['name'];
        }
        if (array_key_exists('age', $data)) {
            $this->age = $data['age'];
        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }
        if (array_key_exists('busNo', $data)) {
            $this->busNumber = $data['busNo'];
        }
        if (array_key_exists('image', $data)) {
            $this->image = $data['image'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

    }

    public function store()
    {

        $driverName = $this->driverName;
        $age = $this->age;
        $phone = $this->phone;
        $busNumber = $this->busNumber;
        $image = $this->image;

        $query = "INSERT INTO driver (`name`, `age`, `phone`, `busNo`, `image`)
VALUES (?,?,?,?,?)";

        $dataArray = array($driverName, $age, $phone, $busNumber, $image);

        $STH = $this->conn->prepare($query);

        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            // return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            //return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function index()
    {

        $sqlQuery = "select * from driver";

        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }
//end of index

    public function view(){

        $sqlQuery = "select * from driver WHERE id=".$this->id;


        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleDriver = $STH->fetch();

        return $singleDriver;


    }
//end of view

    public function update(){

        $driverName = $this->driverName;
        $age = $this->age;
        $phone = $this->phone;
        $busNumber = $this->busNumber;
        $image = $this->image;

        $sqlQuery = "UPDATE driver SET  name= ?,age=?,phone=?,busNo=?,image=? WHERE id =$this->id" ;
        $dataArray = array($driverName,$age,$phone,$busNumber,$image) ;


        $STH = $this->conn->prepare($sqlQuery);


        $result = $STH->execute($dataArray);


        if($result){
            Message::message("Success! Data has been updated Successfully!");
        }
        else{
            Message::message("Error! Data has not been updated");

        }



    }// end of update()
    public function delete(){


        $sqlQuery = "DELETE from driver WHERE id = $this->id;";

        $result = $this->conn->exec($sqlQuery );

        if($result){
            Message::message("Success :) Data has been deleted successfully.");
        }
        else {
            Message::message("Failure :( Data has not been deleted due to an error.");

        }
    }//delete()

    public function indexPaginator($page=1,$itemsPerPage=3){


        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from driver LIMIT $start,$itemsPerPage";


        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }//end of pagination

    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byBusno']) && isset($requestArray['byDriver']) )  $sql = "SELECT * FROM `driver` WHERE  (`name` LIKE '%".$requestArray['search']."%' OR `busNo` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byBusno']) && !isset($requestArray['byDriver']) ) $sql = "SELECT * FROM `driver` WHERE  `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byBusno']) && isset($requestArray['byDriver']) )  $sql = "SELECT * FROM `driver` WHERE  `busNo` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->conn->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()

    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
            //Utility::dd($_allKeywords);
        }
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end

        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->busNo);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->busNo);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end
        return array_unique($_allKeywords);
    }// get all keywords


}