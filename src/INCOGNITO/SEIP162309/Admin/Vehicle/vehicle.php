<?php
namespace App\Admin\Vehicle;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class vehicle extends DB
{
    public $table = "vehicle";
    public $busCategory = "";
    public $busNo = "";
    public $maxSeat = "";
    public $roadInfo = "";
    public $id = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data = array())
    {
        if (array_key_exists('category', $data)) {
            $this->busCategory = $data['category'];
        }
        if (array_key_exists('busNo', $data)) {
            $this->busNo = $data['busNo'];
        }
        if (array_key_exists('maxSeat', $data)) {
            $this->maxSeat = $data['maxSeat'];
        }
        if (array_key_exists('road', $data)) {
            $this->roadInfo = $data['road'];
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

    }

    public function store()
    {

        $busCategory = $this->busCategory;
        $busNo = $this->busNo;
        $maxSeat = $this->maxSeat;
        $roadInfo = $this->roadInfo;

        $query = "INSERT INTO vehicle (`category`, `busNO`, `Max_seat`, `road`)
VALUES (?,?,?,?)";

        $dataArray = array($busCategory, $busNo, $maxSeat, $roadInfo);

        $STH = $this->conn->prepare($query);

        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            // return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            //return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }
//end of store

    public function index()
    {

        $sqlQuery = "select * from vehicle ";

        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }
//end of index
     public function singleindex(){

         $sqlQuery = "select * from vehicle ";

         $STH = $this->conn->query($sqlQuery);

        // $STH->setFetchMode(PDO::FETCH_BOTH);
         $singleData = $STH->fetchAll();
         return $singleData;
     }

    public function view(){

        $sqlQuery = "select * from vehicle WHERE id=".$this->id;


        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleVehicle = $STH->fetch();

        return $singleVehicle;


    }
//end of view
    public function update(){

        $busCategory = $this->busCategory;
        $busNo = $this->busNo;
        $maxSeat = $this->maxSeat;
        $roadInfo = $this->roadInfo;

        $sqlQuery = "UPDATE vehicle SET  category= ?,busNo=?,Max_seat=?,road=? WHERE id =$this->id" ;
        $dataArray = array($busCategory,$busNo,$maxSeat,$roadInfo) ;


        $STH = $this->conn->prepare($sqlQuery);


        $result = $STH->execute($dataArray);


        if($result){
            Message::message("Success! Data has been updated Successfully!");
        }
        else{
            Message::message("Error! Data has not been updated");

        }



    }// end of update()
    public function delete(){

        $sqlQuery = "DELETE from vehicle WHERE id = $this->id;";

        $result = $this->conn->exec($sqlQuery );

        if($result){
            Message::message("Success :) Data has been deleted successfully.");
        }
        else {
            Message::message("Failure :( Data has not been deleted due to an error.");

        }
    }//delete()

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from vehicle  LIMIT $start,$itemsPerPage";


        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }//end of pagination




}