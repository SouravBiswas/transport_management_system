<?php

namespace App\Admin\Notice;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class notice extends DB
{
    public $table = "notice";
    public $id = "";
    public $date= "";
    public $file="";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data = array())
    {
        if (array_key_exists('file', $data)) {
            $this->file = $data['file'];
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if(array_key_exists('date', $data)){
            $this->date = $data['date'];
        }


    }

    public function store()
    {
        //date_default_timezone_set('Asia/Dhaka');

        $file = $this->file;

       // $time = date("dd:mm:yyyy",'h:i:s', time());

      //  echo date('h:i:s a m/d/Y', strtotime($date));


        $query = "INSERT INTO notice (`file`, `date`)
VALUES (?,NOW())";

        $dataArray = array($file);

        $STH = $this->conn->prepare($query);

        $result = $STH->execute($dataArray);

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            // return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored .
                </div>");
            //return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function index()
    {

        $sqlQuery = "select * from notice ";

        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }
//end of index

    public function view(){

        $sqlQuery = "select * from notice WHERE id=".$this->id;


        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $STH->fetch();

        return $singleData;


    }


      public function gettime($timeStamp){


        date_default_timezone_set('Asia/Dhaka');

          $newDateTime = date('h:i A', strtotime($timeStamp));

        $time_ago = strtotime($timeStamp);
        //Utility::dd($time_ago);
        $current_time = time();
        $time_difference = $current_time - $time_ago;
        $seconds = $time_difference;
        $minutes = round($seconds/60);
        $hours = round($seconds/3600);
        $days  = round($seconds/86400);
        $weeks = round($seconds/604800);
        $month = round($seconds/2629440);
        $year = round($seconds/31553280);

        if($seconds<=60){
            return "Just Now";
        }
        else if($minutes<=60){
            if($minutes == 1){
                return "One minutes ago";
            }else{
                return "$minutes minutes ago, $newDateTime";
            }

        }
        else if($hours <=24){
            if($hours==1){

                return "An hour ago, $newDateTime";
            }else{
                return "$hours hours ago, $newDateTime";

            }

        }
        else if($days <=7){
            if($days==1){

                return "Yesterday, $newDateTime";
            }else{
                return "$days days ago, $newDateTime";

            }

        }
        else if($weeks <=4.3){   //52/12
            if($weeks==1){

                return "A week ago, $newDateTime";
            }else{
                return "$weeks weeks ago, $newDateTime";

            }

        }
        else if($month <=12){
            if($month==1){

                return "A month ago, $newDateTime";
            }else{
                return "$month months ago, $newDateTime";

            }

        }
        else {
            if($year==1){
                return "One year ago, $newDateTime";
            }
            else{
                return "$year years ago, $newDateTime";
            }
        }
      }

    public function delete(){

        $sqlQuery = "DELETE from notice WHERE id = $this->id;";

        $result = $this->conn->exec($sqlQuery );

        if($result){
            Message::message("Success :) Data has been deleted successfully.");
        }
        else {
            Message::message("Failure :( Data has not been deleted due to an error.");

        }
    }//delete()

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        if($start<0) $start = 0;


        $sql = "SELECT * from notice  LIMIT $start,$itemsPerPage";


        $STH = $this->conn->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;
    }//end of pagination



}