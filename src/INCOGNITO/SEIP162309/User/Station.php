<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/30/2017
 * Time: 01:18 PM
 */

namespace App\User;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class Station extends DB
{
    public $table = "station";
    public $studentID = "";
    public $station = "";
    public $id = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data = array())
    {
        if (array_key_exists('studentID', $data)) {
            $this->studentID = $data['studentID'];
        }
        if (array_key_exists('stationName', $data)) {
            $this->station = $data['stationName'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

    }

    public function store()
    {

        $studentID = $this->studentID;
        $station = $this->station;
        //Utility::dd($this->station);

        $select = "select student_id from station WHERE student_id = '$studentID'";

        $STH = $this->conn->query($select);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $ID = $STH->fetchAll();
        //Utility::dd($ID);
        $IDs = count($ID);
        if ($IDs > 0) {

            $deleteQuery = "delete from station WHERE student_id ='$studentID'";
            $result = $this->conn->exec($deleteQuery);

            if ($result) {
                //Message::message("Success :) Data has been deleted successfully.");
                $query = "INSERT INTO station (`student_id`, `station`)
VALUES (?,?)";

                $dataArray = array($studentID, $station);

                $STH = $this->conn->prepare($query);

                $result = $STH->execute($dataArray);

                if ($result) {

                    Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Your station confirmed.please check your email .
                </div>");


                    // require_once ('../../../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php');
                    $mail = new \PHPMailer();
                    $mail->IsSMTP();
                    $mail->SMTPDebug  = 0;
                    $mail->SMTPAuth   = true;
                    $mail->SMTPSecure = "ssl";
                    $mail->Host       = "smtp.gmail.com";
                    $mail->Port       = 465;
                    $mail->AddAddress($_SESSION['email']);
                    $mail->Username="incognitophp@gmail.com";
                    $mail->Password="01program01";
                    $mail->SetFrom('incognitophp@gmail.com','IIUC Transport');
                    $mail->AddReplyTo("incognitophp@gmail.com","IIUC Transport");
                    $mail->Subject    = "Bus Schedule";
                    $message =  "
                     
                     Your bus station has been confirmed..
                    ";
                    $mail->MsgHTML($message);
                    $mail->send();


                }
            } else {
                Message::message("This station already selected!!");

            }
            //delete()
        }

        else{

                $query = "INSERT INTO station (`student_id`, `station`)
VALUES (?,?)";

                $dataArray = array($studentID, $station);

                $STH = $this->conn->prepare($query);

                $result = $STH->execute($dataArray);

                if ($result) {

                    Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Your station confirmed.please check your email .
                </div>");


                    // require_once ('../../../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php');
                    $mail = new \PHPMailer();
                    $mail->IsSMTP();
                    $mail->SMTPDebug  = 0;
                    $mail->SMTPAuth   = true;
                    $mail->SMTPSecure = "ssl";
                    $mail->Host       = "smtp.gmail.com";
                    $mail->Port       = 465;
                    $mail->AddAddress($_SESSION['email']);
                    $mail->Username="incognitophp@gmail.com";
                    $mail->Password="01program01";
                    $mail->SetFrom('incognitophp@gmail.com','IIUC Transport');
                    $mail->AddReplyTo("incognitophp@gmail.com","IIUC Transport");
                    $mail->Subject    = "Bus Schedule";
                    $message =  "
                     
                     Your bus station has been confirmed..
                    ";
                    $mail->MsgHTML($message);
                    $mail->send();



                }

            }
        }

    public function chatteshari(){


        $sqlQuery = "select * from station WHERE station='Chatteshwari'";


        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $station1 = $STH->fetch();

        return $station1;



    }

    public function kotowali(){


        $sqlQuery = "select * from station WHERE station='Kotowali'";


        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $station2 = $STH->fetch();

        return $station2;



    }
    public function bohaddarhat(){


        $sqlQuery = "select * from station WHERE station='Bohoddarhat'";


        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $station3 = $STH->fetch();

        return $station3;



    }
    public function agrabad(){


        $sqlQuery = "select * from station WHERE station='Agrabad'";


        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $station4 = $STH->fetch();

        return $station4;



    }
    public function oxygen(){


        $sqlQuery = "select * from station WHERE station='Oxygen Circle, ctg'";


        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $station5 = $STH->fetch();

        return $station5;



    }
    public function sitakundu(){


        $sqlQuery = "select * from station WHERE station='Sitakund,ctg'";


        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $station6 = $STH->fetch();

        return $station6;



    }


}
