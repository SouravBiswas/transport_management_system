<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 7/4/2017
 * Time: 2:33 PM
 */

namespace App\User;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;



class Picture extends DB
{
    public $table = "profile_picture";
    public $image = "";
    public $id = "";
    public $profilePicture="";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($postArray){


        if(array_key_exists("id",$postArray)){
            $this->id = $postArray['id'];
        }

        if(array_key_exists("profilePicture",$postArray)){
            $this->profilePicture = $postArray['profilePicture'];
        }


    }// end of setData()


    public function store(){

        $profilePicture = $this->profilePicture;
        $email = $_SESSION['email'];
        //Utility::dd($email);

        $sqlQuery = "INSERT INTO `profile_picture` (`email`, `profile_picture`) VALUES ( ?, ?);";
        $dataArray = array($email,$profilePicture) ;

        $STH = $this->conn->prepare($sqlQuery);


        $result = $STH->execute($dataArray);


        if($result){
            Message::message("Success! Data has been inserted Successfully!");
        }
        else{
            Message::message("Error! Data has not been inserted.");

        }

    }// end of store()

    public function index(){
             $email = $_SESSION['email'];


        $sqlQuery = "select * from profile_picture WHERE email= '$email'";


        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $singlePicture = $STH->fetch();
       // Utility::dd($singlePicture);
        return $singlePicture;



    }

    public function view(){

        $sqlQuery = "select * from users ";

        $STH = $this->conn->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;

    }


}