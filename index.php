<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="resource/assets/bootstrap/css/bootstrap.min.css">
    <title>Document</title>
    <style>
        #grad {
            background: red; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(left top, red, yellow); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(bottom right, red, yellow); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(bottom right, red, yellow); /* For Firefox 3.6 to 15 */
            background: linear-gradient(to bottom right, red, yellow); /* Standard syntax */
        }
        #image {
            opacity: 1;
            display: block;
            background-color: #a0a0a0;
            height: auto;
            transition: .5s ease;
            backface-visibility: hidden;
        }
         #image:hover {
            opacity: 0.3;
        }


    </style>
</head>
<body style="background-color: #a0a0a0">
 <div class="container" >

    <div class="row" id="grad" style="width: 100%; height: 130px;" >
        <div class="col-lg-2" >
            <img src="resource/assets/img/backgrounds/photo.jpg" alt="iiuc">
        </div>
        <div class="col-lg-10" >
           <h1 style="color: white;font-weight: bolder;margin-left: 65px;font-size: 50px">Transport Management System</h1>
            </div>
    </div>

     <div class="row">
         <div class="col-lg-11" style="height: auto;width: 97.5%;background-color: #253e5a" >
             <marquee style="direction: ltr;color: red;marquee-speed: fast;font-size: 20px"> Welcome to IIUC Transport Management System !!</marquee>
         </div>
     </div>
    </div><br><br>

    <div style="height: 350px;width: 480px;background-color: aliceblue;margin-left: 430px">
        <div class="row">
            <div class="col-lg-6">
                <a href="views/INCOGNITO/SEIP162309/User/Profile/signup.php"> <img class="img-circle" id="image" style="margin-top: 50px;margin-left: 40px" src="resource/assets/img/backgrounds/student.png" alt="student"></a>
              <br> <h5 style="color:#253e5a;font-size: 20px;font-weight: bold;margin-left: 85px ">Student</h5>
            </div>
            <div class="col-lg-6">
                <a href="views/INCOGNITO/SEIP162309/Admin/Profile/signup.php"> <img class="img-circle" id="image" style="margin-top: 51px;margin-left: 20px"; src="resource/assets/img/backgrounds/admin.jpg" alt="admin"></a>
                <br> <h5 style="color:#253e5a;font-size: 20px;font-weight: bold;margin-left: 60px ">Admin</h5>
            </div>
        </div>

    </div>

 </div>

</body>
</html>