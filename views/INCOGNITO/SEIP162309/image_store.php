<?php
if(!isset($_SESSION) )session_start();
use App\Utility\Utility;
require_once('../../../vendor/autoload.php');
//Utility::dd($_SESSION['email']);
$objprofilePicture = new \App\User\Picture();

$fileName = time(). $_FILES['image'] ['name'];

$source = $_FILES['image'] ['tmp_name'];
$destination = "images/".$fileName;

$_POST['profilePicture'] = $fileName;

move_uploaded_file($source, $destination);

$objprofilePicture->setData($_POST);

$objprofilePicture->store();
Utility::redirect('index.php');