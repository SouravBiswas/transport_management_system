<?php
if(!isset($_SESSION) )session_start();
include_once('../../../../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
use App\User\Station;

$objStation = new Station();
//Utility::dd($_SESSION['email']);

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();
$objStation->setData($_SESSION);

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Student Bus Schedule</title>
    <link rel="stylesheet" href="../../../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../../resource/smartmenus-1.0.1/addons/bootstrap/jquery.smartmenus.bootstrap.css">
    <link rel="stylesheet" href="../../../../../resource/smartmenus-1.0.1/css/sm-blue/sm-blue.css">
    <link rel="stylesheet" href="../../../../../resource/bootstrap-select-1.12.2/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="../../../../../resource/smartmenus-1.0.1/css/select.css">
    <link rel="stylesheet" href="../../../../../resource/smartmenus-1.0.1/css/sm-core-css.css">

</head>
<body>
<section id="section">
    <div class="container-fluid">
        <div class="col-sm-12">
            <nav class="main-nav" role="navigation">
                <input id="main-menu-state" type="checkbox" />
                <label class="main-menu-btn" for="main-menu-state">
                    <span class="main-menu-btn-icon"></span> Toggle main menu visibility
                </label>
                <h2 class="nav-brand"><a href="#"> <img src="../../../../../resource/assets/img/backgrounds/International_Islamic_University,_Chittagong_(crest).png" alt="iiuc" class=" img-responsive" id="img">BusSchedule</a></h2>

                <!--navabr start-->
                <ul class="sm sm-blue" id="main-menu">
                    <li><a href="../../index.php">HOME</a></li>
                    <li><a href="#">BUS SCHEDULE</a></li>
                    <li><a href="#">NOTICE BOARD</a></li>
                    <li><a href="#">GET SMS</a></li>
                    <li><a href="#">CONTACT</a></li>
                    <li><a href="#">SETTINGS</a>
                        <ul>
                            <li><a href="#">Edit Profile</a></li>
                            <li><a href="#">Add Fixed SChedule</a></li>
                            <li><a href="#">Deactivate Account</a></li>
                            <li> <a href= "../../User/Authentication/logout.php" > LOGOUT </a></li>
                        </ul>
                    </li>

                </ul>
            </nav>
        </div>
    </div>
</section>
<?php  if(isset($_SESSION['message']) )if($_SESSION['message']!=""){ ?>

    <div  id="message" class=""   style="font-size: 15px; " >
        <center>
            <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                echo "&nbsp;".Message::message();
            }
            Message::message(NULL);
            ?></center>
    </div>
<?php } ?>

<h1 class="text-center text-map">GOOGLE MAP HELPS YOU TO FIND YOUR DESTINATION</h1>
<div class="container-fluid_map">

     <div class="container-select">

             <div class="col-sm-6">
                        <div class="local_selection">


                        <h4 class="text-center">Select your station</h4>
                   <form action="store.php" method="post" class="form-horizontal" id="form_station">

                       <div class="form-group">
                           <label for="userName" class="control-label col-sm-3">
                               Student ID*
                           </label>
                           <div class="col-sm-6">
                               <input type="text" name="studentID" id="userName" class="form-control"/>
                           </div>
                       </div>

                       <div class="form-group">
                           <label for="stationAddress" class="control-label col-sm-3">Station*</label>
               <div class="col-sm-6">


                           <select name="stationName" id="stationAddress" class="selectpicker"  title="select your station">

                               <option value="Chatteshwari Rd,ctg">Chatteshwari Rd,ctg</option>
                               <option value="Kotowali,ctg">Kotowali,ctg</option>
                               <option value="Bahaddarhat,ctg">Bahaddarhat,ctg</option>
                               <option value="Agrabad, ctg">Agrabad, ctg</option>
                               <option value="Oxygen Circle, ctg">Oxygen Circle,ctg</option>
                               <option value="Sitakund,ctg">Sitakund,ctg</option>
                           </select>
                       </div>

                       </div>
                       <div class="form-group">
                           <div class="col-sm-offset-4 col-sm-5">
                               <a href="https://www.google.com"><input type="submit"  class="btn btn-success" id="select_button"/></a>
                           </div>
                       </div>

                   </form>


                            </div>

                        </div>
         <div class="col-sm-6">

             <div id="map" style="width: 100%;">
             </div>
         </div>

 </div>
           </div>







<div class="container-fluid_footer">
    <div class="footer_section">
        <div class="col-sm-12">
            <div class="col-sm-3">
                <div class="contact_footer">
                    <h6 class="footer_header">CONTACT DETAILS</h6>
                    <ul class="list-unstyled">
                        <li><a href="">Address</a></li>
                        <li><a href="">Phone</a></li>
                        <li><a href="">How to get sms</a></li>
                        <li><a href="">Privecy Policy</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="contact_footer">
                    <h6 class="footer_header">SECURITY</h6>
                    <ul class="list-unstyled">
                        <li><a href="">Macafee Verified</a></li>
                        <li><a href="">Know Us</a></li>
                        <li><a href="">Security Certificate</a></li>
                        <li><a href="">Manage your Security</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="contact_footer">
                    <h6 class="footer_header">SOCIAL</h6>
                    <ul class="list-unstyled">
                        <li><a href="">Facebook</a></li>
                        <li><a href="">LinkedIn</a></li>
                        <li><a href="">Google+</a></li>
                        <li><a href="">Twitter</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="contact_footer">
                    <h6 class="footer_header">NOTICE</h6>

                    <ul class="list-unstyled">
                        <li><a href="">Notice Board</a></li>
                        <li><a href="">Previous Notice</a></li>
                        <li><a href="">History</a></li>
                        <li><a href="">About Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="copyright">
        <p class="text-center">
           <i class="fa fa-copyright"></i> Copyright © iiucTeamIncognito.com
        </p>
        </div>
    </div>
</div>





<script src="../../../../../resource/smartmenus-1.0.1/libs/jquery/jquery.js"></script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJZwo1OE-E4Asf_pRe5SZ5fUUjNYlIjy0&callback=initMap">
</script>
<script src="../../../../../resource/smartmenus-1.0.1/jquery.smartmenus.js"></script>
<script src="../../../../../resource/smartmenus-1.0.1/addons/bootstrap/jquery.smartmenus.bootstrap.js"></script>
<script src="../../../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>

<script src="../../../../../resource/bootstrap-select-1.12.2/dist/js/bootstrap-select.min.js"></script>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

</body>


<script type="text/javascript">

    $(function() {
        $('#main-menu').smartmenus({
            subMenusSubOffsetX: 1,
            subMenusSubOffsetY: -8,
            showTimeout: 150,
            subIndicators: true,
            subMenusMinWidth: '200px',
            fadeOut: 250

        });

    });



    $(function() {
        var $mainMenuState = $('#main-menu-state');
        if ($mainMenuState.length) {
            // animate mobile menu
            $mainMenuState.change(function(e) {
                var $menu = $('#main-menu');
                if (this.checked) {
                    $menu.hide().slideDown(250, function() { $menu.css('display', ''); });
                } else {
                    $menu.show().slideUp(250, function() { $menu.css('display', ''); });
                }
            });

            $(window).bind('beforeunload unload', function() {
                if ($mainMenuState[0].checked) {
                    $mainMenuState[0].click();
                }
            });
        }
    });

    $('.selectpicker').selectpicker({
        style: 'btn-primary',
        size: 10,
        showTick : true,
        showContent:true
});

  function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: {lat : 22.4966881,
                lng : 91.7215437}
        });
        directionsDisplay.setMap(map);

      var marker = new google.maps.Marker({
          position: {lat : 22.4966881,
              lng : 91.7215437},
          map: map
      });
        var onChangeHandler = function () {
            calculateAndDisplayRoute(directionsService, directionsDisplay);
        };
        document.getElementById('stationAddress').addEventListener('change', onChangeHandler);

    }
    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        directionsService.route({
                origin: document.getElementById('stationAddress').value,
                destination: {lat : 22.4966881,
                    lng : 91.7215437},

                travelMode: 'DRIVING'
            },
            function (response, status) {
                if (status === 'OK') {
                    directionsDisplay.setDirections(response);
                } else {
                    alert('Directions request failed due to ' + status);
                }
            });


    }





</script>
</html>