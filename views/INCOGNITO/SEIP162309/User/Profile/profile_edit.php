<?php
if(!isset($_SESSION) )session_start();
include_once("../../../../../vendor/autoload.php");
use App\Utility\Utility;
use App\Message\Message;
?>

<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="../../../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../../../resource/smartmenus-1.0.1/css/sm-core-css.css">
    <link rel="stylesheet" href="../../../../../resource/smartmenus-1.0.1/css/sm-blue/sm-blue.css">
    <link rel="stylesheet" href="../../../../../resource/smartmenus-1.0.1/addons/bootstrap/jquery.smartmenus.bootstrap.css">
    <link rel="stylesheet" href="../../../../../resource/smartmenus-1.0.1/css/smartmenu.css">
    <!-- <link rel="stylesheet" href="../../../resource/assets/css/style.css">-->
</head>

<body>
<section id="section">
    <div class="container-fluid">
        <div class="col-sm-12">
            <nav class="main-nav" role="navigation">
                <input id="main-menu-state" type="checkbox" />
                <label class="main-menu-btn" for="main-menu-state">
                    <span class="main-menu-btn-icon"></span> Toggle main menu visibility
                </label>
                <h2 class="nav-brand"><a href="#"> <img src="../../../../../resource/assets/img/backgrounds/photo.jpg" alt="iiuc" class=" img-responsive" id="img">BusSchedule</a></h2>

                <!--navabr start-->
                <ul class="sm sm-blue" id="main-menu">
                    <li><a href="#">HOME</a></li>
                    <li><a href="#">BUS SCHEDULE</a></li>
                    <li><a href="#">NOTICE BOARD</a></li>
                    <li><a href="#">GET SMS</a></li>
                    <li><a href="#">CONTACT</a></li>
                    <li><a href="#">SETTINGS</a>

                        <ul>
                            <li><a href="#">Edit Profile</a></li>
                            <li><a href="#">Add New SChedule</a></li>
                            <li><a href="#">Deactivate Account</a></li>
                            <li> <a href= "../../User/Authentication/logout.php" > LOGOUT </a></li>
                        </ul>
                    </li>


                </ul>
            </nav>
        </div>
    </div>
</section>
<section id="container_section">


    <div class="container">

        <table align="center">
            <tr>
                <td height="100" >

                    <div id="message" >

                        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                            echo "&nbsp;".Message::message();
                        }
                        Message::message(NULL);

                        ?>
                    </div>

                </td>
            </tr>
        </table>
    </div>
    </section>
<section id="profile_section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="profile_form">
                    <form action="edit.php" class="form form form-horizontal" method="post">
                        <div class="form-group">
                            <label for="firstName">First Name</label>
                            <input type="text" class="form-control" name=""
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="footer_section">


        <div class="container-fluid_footer">
            <div class="footer_section">
                <div class="col-sm-12">
                    <div class="col-sm-3">
                        <div class="contact_footer">
                            <h6 class="footer_header">CONTACT DETAILS</h6>
                            <ul class="list-unstyled">
                                <li><a href="">Address</a></li>
                                <li><a href="">Phone</a></li>
                                <li><a href="">How to get sms</a></li>
                                <li><a href="">Privecy Policy</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="contact_footer">
                            <h6 class="footer_header">SECURITY</h6>
                            <ul class="list-unstyled">
                                <li><a href="">Macafee Verified</a></li>
                                <li><a href="">Know Us</a></li>
                                <li><a href="">Security Certificate</a></li>
                                <li><a href="">Manage your Security</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact_footer">
                            <h6 class="footer_header">SOCIAL</h6>
                            <ul class="list-unstyled">
                                <li><a href="">Facebook</a></li>
                                <li><a href="">LinkedIn</a></li>
                                <li><a href="">Google+</a></li>
                                <li><a href="">Twitter</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="contact_footer">
                            <h6 class="footer_header">NOTICE</h6>

                            <ul class="list-unstyled">
                                <li><a href="">Notice Board</a></li>
                                <li><a href="">Previous Notice</a></li>
                                <li><a href="">History</a></li>
                                <li><a href="">About Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="fa-copyright"></div>
                <p class="text-center">
                    Copyright © iiucTeamIncognito.com
                </p>
            </div>
        </div>
</section>

        <!-- Javascript -->
        <script src="../../../../../resource/assets/js/jquery-1.11.1.min.js"></script>
        <script src="../../../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../../../../../resource/assets/js/jquery.backstretch.min.js"></script>
        <script src="../../../../../resource/smartmenus-1.0.1/jquery.smartmenus.min.js"></script>
        <script src="../../../../../resource/smartmenus-1.0.1/addons/bootstrap/jquery.smartmenus.bootstrap.min.js"></script>
        <script src="../../../../../resource/assets/js/scripts.js"></script>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

        <!--[if lt IE 10]>
        <script src="../../../../../resource/assets/js/placeholder.js"></script>
        <![endif]-->

</body>

<script type="text/javascript">
    $('.alert').slideDown("slow").delay(2000).slideUp("slow");

    $(function() {
        $('#main-menu').smartmenus({
            subMenusSubOffsetX: 1,
            subMenusSubOffsetY: -8,
            showTimeout: 150,
            subIndicators: true,
            subMenusMinWidth: '200px',
            fadeOut: 250

        });

    });



    $(function() {
        var $mainMenuState = $('#main-menu-state');
        if ($mainMenuState.length) {
            // animate mobile menu
            $mainMenuState.change(function(e) {
                var $menu = $('#main-menu');
                if (this.checked) {
                    $menu.hide().slideDown(250, function() { $menu.css('display', ''); });
                } else {
                    $menu.show().slideUp(250, function() { $menu.css('display', ''); });
                }
            });

            $(window).bind('beforeunload unload', function() {
                if ($mainMenuState[0].checked) {
                    $mainMenuState[0].click();
                }
            });
        }
    });
</script>

</html>