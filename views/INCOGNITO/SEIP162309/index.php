<?php
if(!isset($_SESSION) )session_start();
include_once('../../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$objPicture= new \App\User\Picture();
$singlePicture = $objPicture->index();

$alldata = $objPicture->view();


//$data = $singlePicture->profile_picture;
//Utility::dd($data);
//Utility::dd($singlePicture);
//Utility::dd($_SESSION['email']);

$obj= new User();
$obj->setData($_SESSION);

$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}
?>


<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/smartmenus-1.0.1/css/sm-core-css.css">
    <link rel="stylesheet" href="../../../resource/smartmenus-1.0.1/css/sm-blue/sm-blue.css">
    <link rel="stylesheet" href="../../../resource/smartmenus-1.0.1/addons/bootstrap/jquery.smartmenus.bootstrap.css">
    <link rel="stylesheet" href="../../../resource/smartmenus-1.0.1/css/smartmenu.css">
   <!-- <link rel="stylesheet" href="../../../resource/assets/css/style.css">-->
</head>

<body>
<section id="section">
    <div class="container-fluid">
        <div class="col-sm-12">
            <nav class="main-nav" role="navigation">
                <input id="main-menu-state" type="checkbox" />
                <label class="main-menu-btn" for="main-menu-state">
                    <span class="main-menu-btn-icon"></span> Toggle main menu visibility
                </label>
                <h2 class="nav-brand"><a href="#"> <img src="../../../resource/assets/img/backgrounds/International_Islamic_University,_Chittagong_(crest).png" alt="iiuc" class=" img-responsive" id="img">BusSchedule</a></h2>

                <!--navabr start-->
                <ul class="sm sm-blue" id="main-menu">
                    <li><a href="#">HOME</a></li>
                    <li><a href="User/Profile/select.php">BUS SCHEDULE</a></li>
                    <li><a href="#">NOTICE BOARD</a></li>
                    <li><a href="#">GET SMS</a></li>
                    <li><a href="#">CONTACT</a></li>
                    <li><a href="#">SETTINGS</a>

                        <ul>
                            <li><a href="#">Edit Profile</a></li>
                            <li><a href="#">Add New SChedule</a></li>
                            <li><a href="#">Deactivate Account</a></li>
                            <li> <a href= "User/Authentication/logout.php" > LOGOUT </a></li>
                        </ul>
                    </li>


                </ul>
            </nav>
        </div>
    </div>
</section>
<section id="container_section">


<div class="container">

    <table align="center" class="table-condensed table-responsive">
        <tr>
            <td height="35" >

                <div id="message" >

                    <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                        echo "&nbsp;".Message::message();
                    }
                    Message::message(NULL);

                    ?>
                </div>

            </td>
        </tr>
    </table>
</div>



<div class="container-fluid_body">
    <div class="col-sm-12">
        <div class="container">
            <div class="row">
             <div class="col-md-7 col-md-offset-3">

                    <div class="panel panel-default">
                        <div class="panel-heading">  <h4 class="text-center" style="font-family: 'Roboto', sans-serif;" >User Profile</h4></div>
                        <div class="panel-body">

                            <div class="box box-info">

                                <div class="box-body">
                                    <div class="col-sm-5">
                                        <?php
                                        if($singlePicture){

                                            echo "
                                            <div  align='center'> <img alt='User Pic 'src='images/$singlePicture->profile_picture' id='profile-image1' class='img-circle img-responsive'></div>
                                           
                                            ";
                                        }
                                        else {
                                            if(!$singlePicture){

                                                echo "
                                        
                                        <div  align='center'> <img alt='User Pic' src='https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg' id='profile-image1' class='img-circle img-responsive'></div>

                                        ";

                                            }



                                        }

                                    ?>
                                        dsdfd



                                            <form action="image_store.php" method="post" enctype="multipart/form-data">

                                            <input type="file" name="image">

                                          <div style="color:#999; margin-top: 10px; font-family: 'Roboto', sans-serif;">


                                                  <button type="submit"  class="btn btn-primary btn-sm" class="form-control">Upload</button>
                                            </div>
                                            <!--Upload Image Js And Css-->

                                            </form>
                                        </div>
                                        <br>

                                        <!-- /input-group -->
                                    </div>
                                    <div class="col-sm-6">
                                        <h4 class="text-center profile_name" style="color:#ffffff; font-family:'Roboto','Roboto', sans-serif ; background-color: #ffffff;">Hi!! <?php echo "$singleUser->first_name $singleUser->last_name"?> </h4></span>

                                    </div>
                                    <div class="clearfix"></div>
                                    <hr style="margin:5px 0 5px 0;">


                                    <div class="col-sm-5 col-xs-6 tital " >First Name:</div><div class="col-sm-7 col-xs-6 "><?php echo $singleUser->first_name?></div>
                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>

                                    <div class="col-sm-5 col-xs-6 tital " >Last Name:</div><div class="col-sm-7"><?php echo $singleUser->last_name?></div>
                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>


                                    <div class="col-sm-5 col-xs-6 tital " >Email:</div><div class="col-sm-7"><?php echo $singleUser->email?></div>

                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>

                                    <div class="col-sm-5 col-xs-6 tital " >Phone no:</div><div class="col-sm-7"><?php echo $singleUser->phone?></div>

                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>

                                    <div class="col-sm-5 col-xs-6 tital " >Address:</div><div class="col-sm-7"><?php echo $singleUser->address?></div>

                                    <div class="clearfix"></div>
                                    <div class="bot-border"></div>


                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->

                            </div>


                        </div>
                    </div>
                </div>
                <script>

                </script>
                 </div>
        </div>

    </div>


   <div class="row">
       <div class="col-sm-12">


<div class="container_description">



           <div class="col-sm-4 div">
               <h4 class="text-center">Noticeboard</h4>
               <hr>
               <p class="p_text  text-justify">Every day the Notice board update.You can get any news or Time for your necessary purpose.University authority always concern about you.Please visit time to time for get any notice from your university.You also see that notice on your station.</p>
               <a href="#" class="pull-right bottom">Edit</a>
           </div>

           <div class="col-sm-4 div">
               <h4 class="text-center">Bus Schedule</h4>
               <hr>
               <p class="p_text text-justify">An automated Email sent you ,when you select your Bus schedule.You can change your schedule anytime.But if you change or update your schedule please confirm delete your old schedule.After getting email you can able to get sms for your schedule over phone.</p>
               <a href="#" class="pull-right bottom">Edit</a>
           </div>
       </div>
   </div>
   </div>

    <div class="container-fluid_important">
        <div class="row">
            <div class="col-sm-12">
                <div class="container_description">

                    <div class="col-sm-4 div">
                        <h4 class="text-center">Add New Schedule</h4>
                        <hr>
                        <p class="p_text  text-justify">You need to full fill your profile from settings section.You can also change or update your profile any time.You must be mention your email, first name and last name clearly.Remember that you can't change your Name.It mustbe match with your Student Id card</p>
                        <a href="#" class="pull-right margin bottom">GET SMS</a>
                    </div>

                    <div class="col-sm-4 div">
                        <h4 class="text-center">Get SMS</h4>
                        <hr>
                        <p class="p_text  text-justify">Every day the Notice board update.You can get any news or Time for your necessary purpose.University authority always concern about you.Please visit time to time for get any notice from your university.You also see that notice on your station.</p>
                        <a href="#" class="pull-right bottom">Edit</a>
                    </div>

                    <div class="col-sm-4 div">
                        <h4 class="text-center">Deactivate Account</h4>
                        <hr>
                        <p class="p_text  text-justify">Every day the Notice board update.You can get any news or Time for your necessary purpose.University authority always concern about you.Please visit time to time for get any notice from your university.You also see that notice on your station.</p>
                        <a href="#" class="pull-right bottom">Edit</a>
                    </div>


                </div>
        </div>
    </div>


</div>


</section>
<div class="container-fluid_footer">
    <div class="footer_section">
        <div class="col-sm-12">
            <div class="col-sm-3">
                <div class="contact_footer">
                    <h6 class="footer_header">CONTACT DETAILS</h6>
                    <ul class="list-unstyled">
                        <li><a href="">Address</a></li>
                        <li><a href="">Phone</a></li>
                        <li><a href="">How to get sms</a></li>
                        <li><a href="">Privecy Policy</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="contact_footer">
                    <h6 class="footer_header">SECURITY</h6>
                    <ul class="list-unstyled">
                        <li><a href="">Macafee Verified</a></li>
                        <li><a href="">Know Us</a></li>
                        <li><a href="">Security Certificate</a></li>
                        <li><a href="">Manage your Security</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="contact_footer">
                    <h6 class="footer_header">SOCIAL</h6>
                    <ul class="list-unstyled">
                        <li><a href="">Facebook</a></li>
                        <li><a href="">LinkedIn</a></li>
                        <li><a href="">Google+</a></li>
                        <li><a href="">Twitter</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="contact_footer">
                    <h6 class="footer_header">NOTICE</h6>

                     <ul class="list-unstyled">
                            <li><a href="">Notice Board</a></li>
                        <li><a href="">Previous Notice</a></li>
                        <li><a href="">History</a></li>
                        <li><a href="">About Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="copyright"> </div>
       <p class="text-center">
           <i class="fa fa-copyright"></i> Copyright iiucTeamIncognito.com
       </p>

    </div>
</div>





<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/smartmenus-1.0.1/jquery.smartmenus.min.js"></script>
<script src="../../../resource/smartmenus-1.0.1/addons/bootstrap/jquery.smartmenus.bootstrap.min.js"></script>
<script src="../../../resource/assets/js/scripts.js"></script>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

<!--[if lt IE 10]>
<script src="../../../resource/assets/js/placeholder.js"></script>
<![endif]-->

</body>

<script type="text/javascript">
    $('.alert').slideDown("slow").delay(2000).slideUp("slow");

    $(function() {
        $('#main-menu').smartmenus({
            subMenusSubOffsetX: 1,
            subMenusSubOffsetY: -8,
            showTimeout: 150,
            subIndicators: true,
            subMenusMinWidth: '200px',
            fadeOut: 250

        });

    });



    $(function() {
        var $mainMenuState = $('#main-menu-state');
        if ($mainMenuState.length) {
            // animate mobile menu
            $mainMenuState.change(function(e) {
                var $menu = $('#main-menu');
                if (this.checked) {
                    $menu.hide().slideDown(250, function() { $menu.css('display', ''); });
                } else {
                    $menu.show().slideUp(250, function() { $menu.css('display', ''); });
                }
            });

            $(window).bind('beforeunload unload', function() {
                if ($mainMenuState[0].checked) {
                    $mainMenuState[0].click();
                }
            });
        }
    });

    $(function() {
        $('#profile-image1').on('click', function() {
            $('#profile-image-upload').click();
        });
    });
</script>

</html>
