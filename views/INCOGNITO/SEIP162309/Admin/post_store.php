<?php
use App\Utility\Utility;
use App\Admin\Notice;
require_once ('../../../../vendor/autoload.php');
$obj = new \App\Admin\Notice\notice();


$obj->setData($_POST);
date_default_timezone_set('Asia/Dhaka');
$obj->store();

Utility::redirect('post_view.php');