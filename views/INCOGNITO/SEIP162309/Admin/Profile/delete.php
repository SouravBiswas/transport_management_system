<?php

if(!isset($_SESSION)) session_start();

include_once ('../../../../../vendor/autoload.php');
use App\Admin\User;
use App\Admin\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();

$obj->setData($_SESSION);
$singleUser = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
$obj->setData($_GET);
$obj->delete();
Utility::redirect('../admin_view.php');

