<?php

if(!isset($_SESSION)) session_start();

include_once ('../../../../../vendor/autoload.php');
use App\Admin\User;
use App\Admin\Auth;
use App\Admin\Driver;
use App\Message\Message;
use App\Utility\Utility;
$obj= new User();
$objDriver = new Driver\driver();

$obj->setData($_SESSION);
$singleUser = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
$objDriver->setData($_GET);
$objDriver->delete();
Utility::redirect('../driver_view.php');

