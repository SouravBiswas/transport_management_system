<?php

if(!isset($_SESSION)) session_start();

include_once ('../../../../../vendor/autoload.php');
use App\Admin\User;
use App\Admin\Auth;
use App\Message\Message;
use App\Utility\Utility;
use App\Admin\Notice;
$objNotice = new Notice\notice();
$obj= new User();

$obj->setData($_SESSION);
$singleUser = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
$objNotice->setData($_GET);
$objNotice->delete();
Utility::redirect('../post_view.php');

