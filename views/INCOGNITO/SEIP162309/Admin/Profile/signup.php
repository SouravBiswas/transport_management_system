<?php
include_once('../../../../../vendor/autoload.php');

   if(!isset($_SESSION) )session_start();
use App\Message\Message;


?>

<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Sign-Up/Login Form</title>
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" href="../../../../../resource/assets/bootstrap/css/bootstrap.min.css">


    <link rel="stylesheet" href="../../../../../resource/assets/css/styles.css">
    <style>
    #grad {
    background: red; /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, red, yellow); /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, red, yellow); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, red, yellow); /* For Firefox 3.6 to 15 */
    background: linear-gradient(to bottom right, red, yellow); /* Standard syntax */
    }
</style>
</head>

<body>
<div class="container">
<div class="row" id="grad" style="width: 100%; height: 130px;" >
    <div class="col-lg-2" >
        <img src="../../../../../resource/assets/img/backgrounds/photo.jpg" alt="iiuc">
    </div>
    <div class="col-lg-10" >
        <h1 style="color: white;font-weight: bolder;margin-left: -130px;margin-top:30px;font-size: 50px">Transport Management System</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-11" style="height: auto;width: 97.5%;background-color: #253e5a">
        <marquee style="direction: ltr;color: red;marquee-speed: fast;font-size: 20px"> Welcome to IIUC Transport Management System !!</marquee>
    </div>
    <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!=""){ ?>

        <div  id="message" class=""   style="font-size: 15px; " >
            <center>
                <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                    echo "&nbsp;".Message::message();
                }
                Message::message(NULL);
                ?></center>
        </div>
    <?php } ?>
</div>

</div>
<div class="form">
    <ul class="tab-group">
        <li class="tab active"><a href="#login">Log In</a></li>
    </ul>

    <div class="tab-content">

        <div id="login">
            <h1>Admin login!</h1>

            <form action="../Authentication/login.php" method="post">

                <div class="field-wrap">
                    <label>
                        Email Address<span class="req">*</span>
                    </label>
                    <input type="email" name="email" required autocomplete="off"/>
                </div>

                <div class="field-wrap">
                    <label>
                        Password<span class="req">*</span>
                    </label>
                    <input type="password" name="password" required autocomplete="off"/>
                </div>

                <p class="forgot"><a href="forgotten.php">Forgot Password?</a></p>

                <button type="submit" class="button button-block"/>Log In</button>

            </form>

        </div>

    </div><!-- tab-content -->

</div> <!-- /form -->
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script src="../../../../../resource/assets/js/index.js"></script>
<script>
    $('.alert').slideDown("slow").delay(5000).slideUp("slow");
</script>


</body>
</html>
