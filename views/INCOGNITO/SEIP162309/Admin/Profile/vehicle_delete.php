<?php

if(!isset($_SESSION)) session_start();

include_once ('../../../../../vendor/autoload.php');
use App\Admin\User;
use App\Admin\Auth;
use App\Admin\Vehicle;
use App\Message\Message;
use App\Utility\Utility;
$obj= new User();
$objVehicle = new Vehicle\vehicle();

$obj->setData($_SESSION);
$singleUser = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
$objVehicle->setData($_GET);
$objVehicle->delete();
Utility::redirect('../vehicle_view.php');

