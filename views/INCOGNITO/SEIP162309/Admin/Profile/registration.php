<?php
include_once('../../../../../vendor/autoload.php');
use App\Admin\User;
use App\Admin\Auth;
use App\Message\Message;
use App\Utility\Utility;
$auth= new Auth();
$status= $auth->setData($_POST)->is_exist();
if($status){
    Message::setMessage("<div class='alert alert-danger'>
    <strong>Taken!</strong> Email has already been taken. </div>");
    //return Utility::redirect($_SERVER['HTTP_REFERER']);
    Utility::redirect('../admin_manage.php');
}else{
   // $_POST['email_token'] = md5(uniqid(rand()));
    $obj= new User();
    $obj->setData($_POST);
    $obj->store();

    
    Utility::redirect('../admin_manage.php');
}
