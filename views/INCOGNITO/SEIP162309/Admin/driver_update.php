<?php

if(!isset($_SESSION)) session_start();

include_once('../../../../vendor/autoload.php');
use App\Admin\User;
use App\Admin\Auth;
use App\Utility\Utility;
use App\Admin\Driver;
$objDriver = new Driver\driver();
$obj= new User();
$obj->setData($_SESSION);

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$objDriver->setData($_POST);
$SingleDriver = $objDriver->view();

$fileName = $SingleDriver->image;

if( $_FILES['image']['name']!="" ){

    $fileName = time(). $_FILES['image'] ['name'];

    $source = $_FILES['image'] ['tmp_name'];

    $destination= "images/".$fileName;

    move_uploaded_file($source,$destination);
}


$_POST['image'] =$fileName;

$objDriver->setData($_POST);

$objDriver->update();

Utility::redirect("driver_view.php");