<?php

if(!isset($_SESSION)) session_start();

include_once('../../../../vendor/autoload.php');
use App\Admin\User;
use App\Admin\Auth;
use App\Utility\Utility;
use App\Admin\Vehicle;
$objVehicle = new Vehicle\vehicle();
$obj= new User();
$obj->setData($_SESSION);

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

$objVehicle->setData($_POST);
$SingleVehicle = $objVehicle->view();

//$objDriver->setData($_POST);

$objVehicle->update();

Utility::redirect("vehicle_view.php");