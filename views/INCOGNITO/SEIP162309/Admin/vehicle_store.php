<?php
use App\Utility\Utility;

require_once ('../../../../vendor/autoload.php');
$obj = new App\Admin\Vehicle\vehicle();

$obj->setData($_POST);

$obj->store();

Utility::redirect('vehicle_view.php');