<?php

if(!isset($_SESSION)) session_start();

include_once ('../../../../vendor/autoload.php');
use App\Admin\User;
use App\Admin\Auth;
use App\Message\Message;
use App\Admin\Notice;
use App\Utility\Utility;
$obj= new User();

$obj->setData($_SESSION);
$singleUser = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
$objNotice = new Notice\notice();
$allData = $objNotice->index();

$objNotice = new Notice\notice();
$allNotice = $objNotice->index();


################# pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;


if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;



$pages = ceil($recordCount/$itemsPerPage);

$someData = $objNotice->indexPaginator($page,$itemsPerPage);
$allData= $someData;


$serial = (  ($page-1) * $itemsPerPage ) +1;



if($serial<1) $serial=1;
####################### pagination code block#1 of 2 end #########################################






?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Bootstrap Admin Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../../resource/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../../resource/assets/bootstrap/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../../../../resource/assets/bootstrap/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../../resource/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body style="">

    <div id="wrapper" >

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">TMS Admin Panel IIUC</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope">&nbsp;Notice Board</i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li style="margin-top: 10px;font-size: 22px">
                            <a href="post_notice.php"><i class="fa fa-fw fa-envelope"></i>Post a New Notice</a>
                        </li>

                        <?php
                        foreach ($allNotice as $notice) {

                            echo "
                    <li class=\"message-preview\">
                        <a href=\"singleNotice_view.php?id=$notice->id\">
                            <div class=\"media\">
                                    <span class=\"pull-left\">
                                        <object style='background-image: url(../../../../resource/assets/img/backgrounds/pdf-navbar.jpg)'  src='../Admin/File/$notice->file' height='30px' width='30px' alt=\"\"></object>
                                    </span>
                                <div class=\"media-body\">
                                    <h5 class=\"media-heading\"><strong>$singleUser->first_name $singleUser->last_name</strong>
                                    </h5>
                                    <p class=\"small text-muted\"><i class=\"fa fa-clock-o\"></i>&nbsp;".$objNotice->gettime($notice->date)."</p>
                                    <p>IIUC Transport Authority</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    
                    ";
                        }
                        ?>
                        <li class="message-footer">
                            <a href="post_view.php">Read All New Messages</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Welcome <?php echo "$singleUser->first_name $singleUser->last_name"?>!  <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="Authentication/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li class="">
                        <a href="admin_manage.php"><i class="fa fa-fw fa-bar-chart-o"></i> Admin Management</a>
                    </li>
                    <li>
                        <a href="driver_manage.php"><i class="fa fa-fw fa-table"></i> All Drivers</a>
                    </li>
                    <li>
                        <a href="vehicle_manage.php"><i class="fa fa-fw fa-edit"></i> Vehicle Management</a>
                    </li>
                    <li class="active">
                        <a href="post_notice.php"><i class="fa fa-fw fa-desktop"></i> Notice Board</a>
                    </li>
                    <li>
                        <a href="student_manage.php"><i class="fa fa-fw fa-wrench"></i>Student Management</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid" style="height: 1500px">
                <?php
                $msg = Message::message();
                if($msg!='')
                    echo  $msg ;
                ?>


                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <a href="post_notice.php">Publish New Post</a> || <a href="post_view.php">View All Post</a>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-bar-chart-o"></i> <a href="admin_manage.php">Admin Manage</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-bar-chart-o"></i> <a href="driver_manage.php">Driver Manage</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-bar-chart-o"></i> <a href="vehicle_manage.php">Vehicle Manage</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-bar-chart-o"></i><a href="post_notice.php">Post Notice</a>
                            </li>


                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <!--table start-->
                    <center><h2>All Published Notice</h2></center><br>
                    <table class="table table-bordered table-striped table-condensed table-responsive">


                        <tr style='font-weight: bold;font-size: 18px;' >
                            <th class="th"><p class="check_text">Serial</p></th>
                            <th  class="th"><p class="check_text">ID</p></th>
                            <th  class="th"><p class="check_text"> Notice</p></th>
                            <th  class="th"><p class="check_text">Publish At</p></th>
                            <th  class="th"><p class="check_text">Action</p></th>
                        </tr>
                        <?php
                        $serial = 1;
                            foreach ($allData as $result) {


   echo "<tr align='center'>";
   echo "<td>$serial</td>" ;
   echo "<td>$result->id</td>" ;
   echo "<td class=''><object style='background-image: url(../../../../resource/assets/img/backgrounds/pdf2.jpg)' id='pdf' type='Application/pdf' class='.glyphicon-file' src='../Admin/File/$result->file' height='50px' width='50px'></object></td>" ;
   echo "<td class='glyphicon glyphicon-time small' style='margin-top: 10px '>".$objNotice->gettime($result->date)."</td>";
      
   echo "<td>" ;
   echo " <a href='download.php?id=$result->id' class='btn btn-primary  btn-sm'>Download</a>";
   echo "&nbsp;<a href='Profile/post_delete.php?id=$result->id' onclick='return confirm_delete()'  class='btn btn-danger  btn-sm'>Delete</a>" ;
   echo "</td>";
   echo "</tr>";

                                $serial++;
                            }

                        ?>

                    </table>

                    </form>


                <!--table end-->
                <!--  ######################## pagination code block#2 of 2 start ###################################### -->
                <div align="left" class="container_pagination" style="width: 100%;height: auto;">
                    <ul class="pagination">

                        <?php

                        $pageMinusOne  = $page-1;
                        $pagePlusOne  = $page+1;


                        if($page>$pages) Utility::redirect("post_view.php?Page=$pages");

                        if($page>1)  echo "<li><a href='post_view.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";


                        for($i=1;$i<=$pages;$i++)
                        {
                            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                        }
                        if($page<$pages) echo "<li><a href='post_view.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

                        ?>

                        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                            <?php
                            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                            ?>
                        </select>
                    </ul>
                </div>
                <!--  ######################## pagination code block#2 of 2 end ###################################### -->





            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../../../../resource/assets/bootstrap/js/plugins/morris/raphael.min.js"></script>
    <script src="../../../../resource/assets/bootstrap/js/plugins/morris/morris.min.js"></script>
    <script src="../../../../resource/assets/bootstrap/js/plugins/morris/morris-data.js"></script>

    <!-- Flot Charts JavaScript -->
    <!--[if lte IE 8]><script src="../../../../resource/assets/bootstrap/js/plugins/flot/excanvas.min.js"></script><![endif]-->
    <script src="../../../../resource/assets/bootstrap/js/plugins/flot/jquery.flot.js"></script>
    <script src="../../../../resource/assets/bootstrap/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="../../../../resource/assets/bootstrap/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="../../../../resource/assets/bootstrap/js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="../../../../resource/assets/bootstrap/js/plugins/flot/flot-data.js"></script>
    <script>
        $('.alert').slideDown("slow").delay(5000).slideUp("slow");
    </script>
    <script>

        function confirm_delete(){

            return confirm("Are You Sure?");

        }

    </script>



</body>

</html>
