<?php

if(!isset($_SESSION)) session_start();

include_once ('../../../../vendor/autoload.php');
use App\Admin\User;
use App\Admin\Auth;
use App\Message\Message;
use App\Admin\Vehicle;
use App\Utility\Utility;
$obj= new User();
$objVehicle = new App\Admin\Vehicle\vehicle();
$objVehicle->setData($_GET);
$singleVehicle = $objVehicle->view();
$obj->setData($_SESSION);
$singleUser = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
$busString = $singleVehicle->category;
$roadString = $singleVehicle->road;

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Bootstrap Admin Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../../resource/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../../resource/assets/bootstrap/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../../../../resource/assets/bootstrap/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../../resource/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <style>
        #page-wrapper{

        }
    </style>
    <![endif]-->

</head>

<body style="">

<div id="wrapper" >

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">TMS Admin Panel IIUC</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                <ul class="dropdown-menu message-dropdown">
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-footer">
                        <a href="#">Read All New Messages</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                <ul class="dropdown-menu alert-dropdown">
                    <li>
                        <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">View All</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Welcome <?php echo "$singleUser->first_name $singleUser->last_name"?>!  <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="Authentication/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                </li>
                <li class="">
                    <a href="admin_manage.php"><i class="fa fa-fw fa-bar-chart-o"></i> Admin Management</a>
                </li>
                <li class="active">
                    <a href="driver_manage.php"><i class="fa fa-fw fa-table"></i> All Drivers</a>
                </li>
                <li>
                    <a href="vehicle_manage.php"><i class="fa fa-fw fa-edit"></i> Vehicle Management</a>
                </li>
                <li>
                    <a href="post_notice.php"><i class="fa fa-fw fa-desktop"></i> Notice Board</a>
                </li>
                <li>
                    <a href="student_manage.php"><i class="fa fa-fw fa-wrench"></i> Student Management</a>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid" style="height: 1200px">
            <?php
            $msg = Message::message();
            if($msg!='')
                echo  $msg ;
            ?>

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <a href="vehicle_manage.php">Add New Vehicle</a> || <a href="vehicle_view.php">View All vehicle</a>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                        </li>
                        <li class="">
                            <i class="fa fa-dribbble"></i><a href="admin_manage.php">Admin Manage</a>
                        </li>
                        <li class="">
                            <i class="fa fa-dribbble"></i><a href="driver_manage.php">Driver Manage</a>
                        </li>
                        <li class="">
                            <i class="fa fa-dribbble"></i><a href="vehicle_manage.php">Vehicle Manage</a>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <!--form start-->
            <form role="form" action="vehicle_update.php" method="post" enctype="multipart/form-data">

                <div class="form-group">
                    <label>Select Bus Catagory</label>
                    <select name="category" id="category" class="form-control">
                        <option>Choose a bus type..</option>
                        <option value="IIUC Bus" <?php if($busString=="IIUC Bus") echo "selected"?>>IIUC Bus</option>
                        <option value="Local Bus" <?php if($busString=="Local Bus") echo "selected"?>>Local Bus</option>
                        <option value="Double Dackher" <?php if($busString=="Double Dackher") echo "selected"?>>Double Dackher</option>
                    </select>
                </div>


                <div class="form-group">
                    <label>Vehicle Number</label>
                    <input type="text" value="<?php echo $singleVehicle->busNo?>" name="busNo" class="form-control">
                </div>

                <div class="form-group">
                    <label>Maximum Seat Capability</label>
                    <input type="text" id="maxSeat" value="<?php echo $singleVehicle->Max_seat?>" name="maxSeat" placeholder="maximum seat capability"  class="form-control">
                </div>
                <div class="form-group">
                    <label>Selects Road/Station</label>
                    <select name="road" class="form-control">
                        <option>Choose a Road or Station</option>
                        <option value="Chatteshori,Chakbazar."<?php if($roadString=="Chatteshori,Chakbazar.") echo "selected"?>>Chatteshori,Chakbazar.</option>
                        <option value="Kotowali,Ctg"<?php if($roadString=="Kotowali,Ctg") echo "selected"?>>Kotowali,Ctg</option>
                        <option value="Agrabadh,Ctg"<?php if($roadString=="Agrabadh,Ctg") echo "selected"?>>Agrabadh,Ctg</option>
                        <option value="Oxyzen,Ctg"<?php if($roadString=="Oxyzen,Ctg") echo "selected"?>>Oxyzen,Ctg</option>
                        <option value="Bohordarhat,Ctg"<?php if($roadString=="Bohordarhat,Ctg") echo "selected"?>>Bohordarhat,Ctg</option>
                        <option value="Sitakundo,Ctg"<?php if($roadString=="Sitakundo,Ctg") echo "selected"?>>Sitakundo,Ctg</option>
                    </select>
                </div>
                <input type="hidden" name="id" value="<?php echo $singleVehicle->id?>">

                <button type="submit" class="btn btn-primary">Update </button>

            </form>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../../../../resource/assets/bootstrap/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="../../../../resource/assets/bootstrap/js/plugins/morris/raphael.min.js"></script>
<script src="../../../../resource/assets/bootstrap/js/plugins/morris/morris.min.js"></script>
<script src="../../../../resource/assets/bootstrap/js/plugins/morris/morris-data.js"></script>

<!-- Flot Charts JavaScript -->
<!--[if lte IE 8]><script src="../../../../resource/assets/bootstrap/js/plugins/flot/excanvas.min.js"></script><![endif]-->
<script src="../../../../resource/assets/bootstrap/js/plugins/flot/jquery.flot.js"></script>
<script src="../../../../resource/assets/bootstrap/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="../../../../resource/assets/bootstrap/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="../../../../resource/assets/bootstrap/js/plugins/flot/jquery.flot.pie.js"></script>
<script src="../../../../resource/assets/bootstrap/js/plugins/flot/flot-data.js"></script>
<script>
    $('.alert').slideDown("slow").delay(5000).slideUp("slow");
</script>

</body>

</html>
