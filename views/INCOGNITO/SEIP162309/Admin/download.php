<?php

require_once ('../../../../vendor/autoload.php');


use App\Admin\Notice;

$objNotice = new Notice\notice();
$objNotice->setData($_GET);
$singleNotice = $objNotice->view();
$path = $singleNotice->file;
$path1 = 'File/'.$path;
header('Content-Description: File Transfer');
header('Content-Type: application/pdf; charset=utf-8');
header('Content-Disposition: attachment; filename='.basename($path1));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Length: ' . filesize($path1));
ob_clean();
flush();
readfile($path1);
