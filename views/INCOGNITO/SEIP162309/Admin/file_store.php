<?php
use App\Utility\Utility;

require_once("../../../../vendor/autoload.php");

$objFile  = new App\Admin\Notice\notice();

$fileName = time(). $_FILES['file']['name'] ;

$source = $_FILES['file'] ['tmp_name'];
$destination = "File/".$fileName;

$_POST['file'] = $fileName;

move_uploaded_file($source, $destination);

$objFile->setData($_POST);

$objFile->store();

Utility::redirect('post_view.php');