<?php

if(!isset($_SESSION)) session_start();

include_once ('../../../../vendor/autoload.php');
use App\Admin\User;
use App\Admin\Auth;
use App\Message\Message;
use App\Admin\Driver;
use App\Utility\Utility;
$obj= new User();
$objDriver = new Driver\driver();
$objDriver->setData($_GET);
$singleDriver = $objDriver->view();
$obj->setData($_SESSION);
$singleUser = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();
$busString = $singleDriver->busNo;



?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Bootstrap Admin Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../../resource/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../../resource/assets/bootstrap/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../../../../resource/assets/bootstrap/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../../resource/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <style>
        #page-wrapper{

        }
    </style>
    <![endif]-->

</head>

<body style="">

<div id="wrapper" >

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">TMS Admin Panel IIUC</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                <ul class="dropdown-menu message-dropdown">
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-preview">
                        <a href="#">
                            <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                <div class="media-body">
                                    <h5 class="media-heading"><strong>John Smith</strong>
                                    </h5>
                                    <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="message-footer">
                        <a href="#">Read All New Messages</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                <ul class="dropdown-menu alert-dropdown">
                    <li>
                        <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                    </li>
                    <li>
                        <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">View All</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Welcome <?php echo "$singleUser->first_name $singleUser->last_name"?>!  <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="Authentication/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li>
                    <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                </li>
                <li class="">
                    <a href="admin_manage.php"><i class="fa fa-fw fa-bar-chart-o"></i> Admin Management</a>
                </li>
                <li class="active">
                    <a href="driver_manage.php"><i class="fa fa-fw fa-table"></i> All Drivers</a>
                </li>
                <li>
                    <a href="vehicle_manage.php"><i class="fa fa-fw fa-edit"></i> Vehicle Management</a>
                </li>
                <li>
                    <a href="post_notice.php"><i class="fa fa-fw fa-desktop"></i> Notice Board</a>
                </li>
                <li>
                    <a href="student_manage.php"><i class="fa fa-fw fa-wrench"></i> Student Management</a>
                </li>

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid" style="height: 800px">
            <?php
            $msg = Message::message();
            if($msg!='')
                echo  $msg ;
            ?>

            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <a href="driver_manage.php">Add New Driver</a> || <a href="driver_view.php">View All Driver</a>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                        </li>
                        <li class="">
                            <i class="fa fa-dribbble"></i><a href="admin_manage.php">Admin Manage</a>
                        </li>
                        <li class="active">
                            <i class="fa fa-dribbble"></i><a href="driver_manage.php">Driver Manage</a>
                        </li>
                    </ol>
                </div>
            </div>
            <!-- /.row -->

            <!--form start-->
            <form role="form" action="driver_update.php" method="post" enctype="multipart/form-data">

                <div class="form-group">
                    <label>Driver Name</label>
                    <input type="text" name="name" value="<?php echo $singleDriver->name?>" class="form-control">
                   </div>

                <div class="form-group">
                    <label>Age</label>
                    <input type="text" name="age" value="<?php echo $singleDriver->age?>" class="form-control">
                    </div>

                <div class="form-group">
                    <label>Phone Number</label>
                    <input type="text" name="phone" value="<?php echo $singleDriver->phone?>" class="form-control" placeholder="Mobile number">
                </div>
                <div class="form-group">
                    <label>Selects A Bus</label>
                    <select name="busNo" class="form-control">
                        <option>Choose a bus number</option>
                        <option value="A-100230" <?php if($busString=="A-100230") echo "selected";?>>A-100230</option>
                        <option value="B-2001520" <?php if($busString=="B-2001520") echo "selected";?>>B-2001520</option>
                        <option value="C-200450" <?php if($busString=="C-200450") echo "selected";?> >C-200450</option>
                        <option value="D-120230" <?php if($busString=="D-120230") echo "selected";?>>D-120230</option>
                        <option value="E-120320" <?php if($busString=="E-120320") echo "selected";?>>E-120320</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Upload A Picture</label>
                    <input name="image" type="file" accept=".png,.jpg,.jpeg">
                    <img src="images/<?php echo $singleDriver->image ?>" alt="image" height="300px" width="300px" class="img-responsive">

                </div>

                <input type="hidden" name="id" value="<?php echo $singleDriver->id?>">

                <button type="submit" class="btn btn-primary">Update </button>

            </form>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="../../../../resource/assets/bootstrap/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="../../../../resource/assets/bootstrap/js/plugins/morris/raphael.min.js"></script>
<script src="../../../../resource/assets/bootstrap/js/plugins/morris/morris.min.js"></script>
<script src="../../../../resource/assets/bootstrap/js/plugins/morris/morris-data.js"></script>

<!-- Flot Charts JavaScript -->
<!--[if lte IE 8]><script src="../../../../resource/assets/bootstrap/js/plugins/flot/excanvas.min.js"></script><![endif]-->
<script src="../../../../resource/assets/bootstrap/js/plugins/flot/jquery.flot.js"></script>
<script src="../../../../resource/assets/bootstrap/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="../../../../resource/assets/bootstrap/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="../../../../resource/assets/bootstrap/js/plugins/flot/jquery.flot.pie.js"></script>
<script src="../../../../resource/assets/bootstrap/js/plugins/flot/flot-data.js"></script>
<script>
    $('.alert').slideDown("slow").delay(5000).slideUp("slow");
</script>

</body>

</html>
