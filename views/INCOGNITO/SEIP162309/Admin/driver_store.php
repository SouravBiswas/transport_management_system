<?php
use App\Utility\Utility;

require_once ('../../../../vendor/autoload.php');
$obj = new App\Admin\Driver\driver();

$fileName = time(). $_FILES['image'] ['name'];

$source = $_FILES['image'] ['tmp_name'];
$destination = "images/".$fileName;

$_POST['image'] = $fileName;

move_uploaded_file($source, $destination);

$obj->setData($_POST);

$obj->store();

Utility::redirect('driver_view.php');