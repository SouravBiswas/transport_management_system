-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2017 at 08:23 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `user-management`
--
CREATE DATABASE IF NOT EXISTS `user-management` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `user-management`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(100) NOT NULL,
  `first_name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_name` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone` int(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email_verified` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`) VALUES
(7, 'zarif', 'Rahman', 'zarif@gmail.com', 'd0970714757783e6cf17b26fb8e2298f', 2147483647, 'sdsdsad f efe fe ff', 'Yes'),
(8, 'dsfgsdfg', 'dgg', 'sdgdfg@sfdsfs', 'c20ad4d76fe97759aa27a0c99bff6710', 1, 'sdfdsf', 'Yes'),
(12, 'Muhtasim', 'chowdhury', 'mu@gmail.com', 'd05e4ea727b6f28633b4567a5d37ee44', 123658, 'D.C.Road', 'Yes'),
(13, 'fhygh', 'tgfgd', 'ddfg@fsdfsdds', 'd6e9c56d7f078d298ed4695d899effbe', 301, 'sdfdf', 'Yes'),
(14, 'sourav', 'biswas', 'bsripon1@gmail.com', '4b08df9a969ba4dda3a8f1441b5b912e', 1836129290, 'D.C.Road', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `id` int(100) NOT NULL,
  `name` text NOT NULL,
  `age` int(100) NOT NULL,
  `phone` int(100) NOT NULL,
  `busNo` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`id`, `name`, `age`, `phone`, `busNo`, `image`) VALUES
(8, 'Mohammad Iqbal', 40, 1836203695, 'B-2003021', '1498282624iqbal.jpg'),
(9, 'Mohammad Abul Kalam', 50, 167802365, 'D-1502320', '1498282650abul kalam.jpg'),
(10, 'Mohammad Jamal Bhuiya', 48, 171756023, 'C-1520120', '1498282677mohammad.jpg'),
(11, 'Idris Mia', 35, 12365894, 'C-230250', '1498282739Hydrangeas.jpg'),
(12, 'Mir Kashem Ali', 39, 12365891, 'C-230250', '1498282815Chrysanthemum.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` int(100) NOT NULL,
  `file` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `file`, `date`) VALUES
(15, 'IIUC Transport.pdf', '2017-06-25 01:10:03'),
(17, '1498339689personHobby.pdf', '2017-06-25 03:28:09'),
(18, '1498450052BookTitle.pdf', '2017-06-26 10:07:32'),
(19, '1498454837BookTitle_2.pdf', '2017-06-26 11:27:17'),
(20, '1498471370IIUC Transport.pdf', '2017-06-26 16:02:50'),
(35, '1499268493IIUC Transport (7).pdf', '2017-07-05 21:28:13'),
(36, '1499268828IIUC Transport (8).pdf', '2017-07-05 21:33:48'),
(37, '1499316874IIUC Transport (1).pdf', '2017-07-06 10:54:34'),
(38, '1499318330IIUC Transport (2).pdf', '2017-07-06 11:18:50');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `profile_picture` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `email`, `profile_picture`) VALUES
(2, 'bsripon1@gmail.com', '1499244504Chrysanthemum.jpg'),
(3, 'greenhornetstart786@gmail.com', '1499249210Penguins.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `station`
--

CREATE TABLE `station` (
  `id` int(100) NOT NULL,
  `student_id` varchar(100) NOT NULL,
  `station` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `station`
--

INSERT INTO `station` (`id`, `student_id`, `station`) VALUES
(6, 'c120325', 'Oxygen'),
(7, 'c1452520', 'Kotowali'),
(13, 'c141202', 'Agrabad'),
(15, 'c120235', 'Chatteshwari'),
(17, 'c141041', 'Chatteshwari Rd,ctg'),
(18, 'c145522', 'Agrabad, ctg'),
(19, 'c145456', 'Sitakund,ctg'),
(20, 'c44524', 'Kotowali,ctg'),
(21, 'c145266', 'Oxygen Circle, ctg'),
(23, 'c141025', 'Bahaddarhat,ctg'),
(24, 'c145236', 'Oxygen Circle, ctg'),
(25, 'c145263', 'Sitakund,ctg'),
(26, 'c145230', 'Bahaddarhat,ctg'),
(28, '', 'Oxygen Circle, ctg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(333) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`) VALUES
(17, 'Test', 'User', 'tushar.chowdhury@gmail.com', '202cb962ac59075b964b07152d234b70', '01711111111', 'Chittagong', 'Yes'),
(18, 'sdjf', 'lksdjf', 'tusharbd@gmail.com', 'caf1a3dfb505ffed0d024130f58c5cfa', '5235', 'dfgdg', 'Yes'),
(19, 'asfds', 'sdfgs', 'x@y.z', '202cb962ac59075b964b07152d234b70', '4545', 'sfsj', '4ae15d1c46f25be8db9d07061463c5f0'),
(30, 'xyz', 'xy', 'x@gmail.com', 'd0970714757783e6cf17b26fb8e2298f', '012356896', 'sadsadsad', '891e29d4e4451ad957121a44f073803d'),
(31, 'sourav', 'biswas', 'bsripon1@gmail.com', '4b08df9a969ba4dda3a8f1441b5b912e', '01635183372', 'hngh', 'Yes'),
(32, 'saikat ', 'Khan', 'ansaikat889@gmail.com', '25d55ad283aa400af464c76d713c07ad', '01843140972', 'sfsdf', 'Yes'),
(34, 'xdfsx', 'dfgvd', 'dsfdf@gmail.com', '4b08df9a969ba4dda3a8f1441b5b912e', '45343', '54365432', '0773eba4a332363665b614f78364ef31'),
(41, 'Incognito', 'Php', 'incognitophp@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '01866040165', 'BITM', 'Yes'),
(45, 'ryan', 'mahmud', 'greenhornetstart786@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '6767956546', 'fgjdgyjkfj', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `id` int(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `busNo` varchar(100) NOT NULL,
  `Max_seat` int(100) NOT NULL,
  `road` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`id`, `category`, `busNo`, `Max_seat`, `road`) VALUES
(6, 'Local Bus', 'A-6023021', 35, 'Chatteshori,Chakbazar.'),
(7, 'Double Dackher', 'B-102520', 70, 'Kotowali,Ctg'),
(8, 'IIUC Bus', 'A-100200', 50, 'Bohordarhat,Ctg'),
(9, 'IIUC Bus', 'B-2003021', 50, 'Kotowali,Ctg'),
(10, 'Local Bus', 'V-1024012', 35, 'Kotowali,Ctg'),
(11, 'Local Bus', 'C-1520120', 35, 'Oxyzen,Ctg'),
(12, 'Local Bus', 'D-1502320', 35, 'Sitakundo,Ctg'),
(13, 'Double Dackher', 'E-12520350', 65, 'Sitakundo,Ctg'),
(14, 'Double Dackher', 'D-1203520', 70, 'Oxyzen,Ctg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `station`
--
ALTER TABLE `station`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `busNo` (`busNo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `station`
--
ALTER TABLE `station`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
